import Header from '../components/header/Header';
import LatestMedium from '../components/medium/LatestMedium';
import ProjectsPreview from '../components/projects/preview/ProjectsPreview';

const Home = () => (
  <>
    <Header/>
    <LatestMedium/>
    <ProjectsPreview/>
  </>
);

export default Home