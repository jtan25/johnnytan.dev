import React from 'react';
import Project from '../components/projects/view/Project';

const ProjectView = (props) => {
  const id = props.query.id;
  return (
    <Project id={id} />
  )
}

export default ProjectView;