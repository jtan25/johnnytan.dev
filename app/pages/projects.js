import React from 'react';
import ProjectsDirectory from '../components/projects/directory/ProjectsDirectory';

const Projects = () => {
  return (
    <ProjectsDirectory/>
  )
}

export default Projects;