import React from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const myEmail = 'jhnnytny@gmail.com';

const EmailMeContainer = styled.div`
    text-align: center;
    margin-top: 2rem;
`;

const EmailMeButton = styled.a`
    background-color: rgb(52, 68, 241);
    border-radius: 0.65rem;
    color: rgb(255, 255, 255);
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    padding-left: 4.15em;
    padding-right: 4.15em;
    text-align: center;
    width: auto;
    padding: 1em 3em;
    transition: all .25s;
    display: inline-block;
    border: 1px solid grey;
    cursor: pointer;
    border: none;
    text-decoration: none;
`;

const EmailMe = () => {
  return(
    <EmailMeContainer>
      <EmailMeButton href={`mailto:${myEmail}`}>
        <FontAwesomeIcon icon={['fa','circle-notch']} color="white" width="12" style={{marginRight: '4px'}} />
          Email
      </EmailMeButton>
    </EmailMeContainer>
  )
}

export default EmailMe;