export const projects = [
  {
    id: '1',
    type: '2',
    title: 'Not Flickr',
    oneLiner: 'Photo Sharing with Blockstack',
    summary:'',
    logo: '',
    icon: '',
    tags: ['Solidity','React',],
    directLink:'https://medium.com/@tanjohnny/5-takeaways-from-my-recent-hackathons-25746ea311f3',
    website: '',
    playStore: '',
    appStore: '',
    description:'',
  },
  {
    id: '2',
    type: '2',
    title: 'Nudge',
    oneLiner:'Decentralized goal setting platform where users can bet on themselves and on each other',
    summary: '',
    logo: '',
    icon: '',
    tags: ['Concept', 'Blockchain'],
    website: '',
    playStore: '',
    appStore: '',
    description:'# Nudge \nNudge was a goal setting platform that [Will](https://www.linkedin.com/in/wmkelly/), [Mike](https://www.linkedin.com/in/mtzhai/), and I started at the 2018 MIT Bitcoin Hackathon. Nudge\'s first use case was using smart contracts to faciliate following scenario:  \n \
    \n ``` \n Alice and Bob want to lose weight. \
    \nThey each run 10 miles a week. \
    \nThey both decide to stake $200 USD each week where the winner takes all. If there is a tie then they both receive their money back. \
    \nThey decide to employ their friend Charlie to referee or moderate each week. And they agree to give him a % of the staked amount for refereeing. \
    \n ``` \
    \nWe felt that loss aversion and accountability were essential to accomplishing goals. And developing this concept further we presented the idea of creating a market where users can bet on themselves or allow others to bet against them. \
    As a result a self regulated community with infinite possibilities is created. \
    \n # Where is it now? \nI loved this concept but my teammates didn\'t. This idea ended up just being put into backlog and hasn\'t been touched since (I am taking a break from working on crypto projects solo). \
    ',
  },
  {
    id: '3',
    type: '2',
    title: 'MA Bounties',
    oneLiner: 'ERC721 for completing bounties',
    summary:'',
    logo: '',
    icon: '',
    tags: ['Solidity', 'JavaScript'],
    website: '',
    directLink: 'https://medium.com/@tanjohnny/bounties-ad41a3b0e1c2',
    playStore: '',
    appStore: '',
    description:'',
  },
  {
    id: '4',
    type: '2',
    title: 'Augural',
    oneLiner: 'The Graph + Augur',
    summary:'',
    logo: '',
    icon: '',
    tags: ['GraphQL', 'JavaScript'],
    website: 'https://github.com/jtan25/Augural',
    playStore: '',
    appStore: '',
    description:'[Eli](https://www.linkedin.com/in/elihanover/) and I recently came across [TheGraph](https://thegraph.com/). We\'ve always been interested in the [Augur](https://www.augur.net/) project but were not so impressed by the current applications built on top of it. We wanted to start tracking and doing analytics for users on Augur. We explored the Augur contract and used TheGraph\'s api to query the event data that gets emitted. We eventually found that this didn\'t grab all the information that we needed. We concluded that the [Augur API](https://github.com/AugurProject/augur.js) better suited our needs.',
  },
  {
    id: '5',
    type: '1',
    title: 'Rent the driveway',
    oneLiner: 'I can\'t find parking in Brooklyn',
    summary:'I wanted to explore what options were there for recording audio on the browser. I created a project to install and test various options. It ended up being this web app.',
    logo: '',
    icon: '',
    tags: ['Idea'],
    website: '',
    playStore: '',
    appStore: '',
    description:'# Parking in Brooklyn \
    \nParking in Brooklyn can be cumbersome because there are no parking lots and a ton of driveways for residents. I was in Bay Ridge the other week and faced this issue. However what I noticed was that a lot of these driveways are actually empty. What if we allowed residents to rent out their driveways for parking to others. A system would be needed in place to show lots that are available and the time permitted to park there. And a third party can be used to tow or move cars as necessary. \
    ',
  },
  {
    id: '6',
    type: '1',
    title: 'Google Search Engineer',
    oneLiner: 'We tend to google everything',
    summary:'I wanted to explore what options were there for recording audio on the browser. I created a project to install and test various options. It ended up being this web app.',
    logo: '',
    icon: '',
    tags: ['Idea'],
    website: '',
    playStore: '',
    appStore: '',
    description:'# Google Search + Development \
    \nGoogle search is a software engineers best friend. The running joke is how good of an engineer you are depends on how great you are at googling. Although this is a joke it is true that we tend to use google to find query for answers to problems others have faced. The following example are the steps we normally would take:  \
    \n1. Google search problem quickly on separate browser tab. \
    \n2. Find a stackoverflow link (e.g. medium, quora, github, etc) related to our question. \
    \n3. From there we read and select the best answer. \
    \n4. We might copy over some code or example to our co de editors. \
    \n# What I want \
    \nWhat if we were to automatically save and store the information from the process above. Perhaps we can even give feedback on the answer or solution we choose. We can make note of what worked and what didn\'t. Then we can even share or search for this solution later incase that we forget. An example of what is stored: \
    \n``` \
    \nGoogle Search: wordcloud from csv python \
    \nLink: https://stackoverflow.com/questions/46202600/creating-word-cloud-in-python-from-column-in-csv-file \
    \nCopied the following: some code snippet... \
    \nNotes: wordcloud csv python1 \
    \n``` \
    ',
  },
  {
    id: '7',
    type: '0',
    title: 'Prodblems',
    oneLiner: 'Product + Problems',
    summary:'The world is full of ideas...',
    logo: '',
    icon: '',
    tags: ['React', 'Nextjs'],
    website: '',
    playStore: '',
    appStore: '',
    description:'# Ideas are everywhere \
    \nThe discussion of ideas, startups, or products exist on so many platforms. This space is huge and its hard to keep up with everything that goes on. I am personally subscribed to multiple newsletter for news on what goes on. I\'ve wanted to try to better organize and aggregate all this information. The following sites are what I\'ve come up with to look at: \
    \n* AngelList \
    \n* IndieHackers \
    \n* Product Hunt \
    \n* TrendHunter \
    \n* Reddit \
    \n* SharkTank \
    \n* BetaList \
    \n* Hacker News \
    \n* Twitter \
    \n* Crunchbase \
    \n* TechCrunch \
    ',
  },
  {
    id: '8',
    type: '2',
    title: 'Voice recording on the browser',
    oneLiner: 'A look at voice recording on the web',
    summary:'I wanted to explore what options were there for recording audio on the browser. I created a project to install and test various options. It ended up being this web app.',
    logo: '',
    icon: '',
    tags: ['React'],
    website: '',
    directLink: 'https://voicerecorder-e3277.web.app/',
    playStore: '',
    appStore: '',
    description:'',
  },
  {
    id: '9',
    type: '0',
    title: 'This website',
    oneLiner: 'I wanted to build a portfolio builder site for developers...',
    summary:'Seems like every developer makes their own portfolio sites or personal websites. Linkedin doesn\'t seem that developer friendly. And there exist behance and dribbble for designers. Github is great for looking at code but how often do we look at the contributors page?',
    logo: '',
    icon: '',
    tags: ['JavaScript', 'Web', 'React', 'Nextjs'],
    website: '',
    playStore: '',
    appStore: '',
    description:'# Linkedin isn\'t made for developers.... \
    \nI\'ve noticed Linkedin becoming more and more popular over the years as a place to establish your professional identity publicly. But for developers I think it does a terrible job of showcasing who they are and their work. Other platforms such as Github or Product Hunt don\'t highlight the identity of the person (people on those platform care solely about the code repo or product). Because of all of this some developers end up building their own website which needs to be found (e.g. via Google Search) or obtained (e.g. shared on Linkedin). And tech recruiters often have a hard time guaging potential candidates as these platforms don\'t showcase developers accurately. \
    \n \
    \n # What I started doing and what I want to achieve... \
    \nI started writing bare bones components that developers would want on their site. This website is a shell or example of some of the components that I have worked on. My goal is to provide all the necessary components to make up a developers ideal Linkedin profile. By doing so they can all remain on a single platform to be found and can showcase who they are and their work better. \
    ',
  },
];