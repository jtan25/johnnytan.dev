import React from 'react';
import Link from 'next/link';
import styled from 'styled-components';
import ProjectsPreviewItem from './ProjectsPreviewItem';
import { projects } from '../projects';

const Section = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    position: relative;
    margin: 0;
    padding: 6rem 1rem;
`;

const ProjectsPreviewContainer = styled.div`
    width: 1000px;
    opacity: 1!important;
    transform: none!important;
    max-width: 100%;
    margin: 0 auto;
    z-index: 1;
    position: relative;
    animation: fadeFromAbove 1s;
`;

const ProjectsPreviewTitle = styled.h2`
    font-size: 2.5rem;
    font-weight: 900;
    line-height: 1;
    text-align: center;
    margin-bottom: 4rem;
`;

const ProjectsPreviewSubtitle = styled.p`
    font-size: 1.3rem;
    font-weight: 400;
    line-height: 1.75;
    text-align: center;
    margin: -3rem 0 2rem;
`;

const ProjectsPreviewMessage = styled.p`
    font-size: 1.3rem;
    font-weight: 400;
    line-height: 1.75;
    text-align: center;
`;

const ProjectsPreviewItemsContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  text-align: center;
  width: 1100px;
  max-width: 100%;
  margin: 0 auto;
  z-index: 1;
  position: relative;
`;



function ProjectsPreview() {
  return (
    <Section>
      <ProjectsPreviewContainer>
        <ProjectsPreviewTitle>
          Latest Project Updates
        </ProjectsPreviewTitle>
        <ProjectsPreviewSubtitle>
          From my <Link href='/projects'><a style={{textDecoration: 'none', color:'black', fontWeight: 700}}>projects directory</a></Link>
        </ProjectsPreviewSubtitle>
          <ProjectsPreviewItemsContainer>{projects.slice(projects.length -2 , projects.length).map((project,i) => <ProjectsPreviewItem props={project} key={i}/>)}</ProjectsPreviewItemsContainer>
      </ProjectsPreviewContainer>
    </Section>
  )
}

export default ProjectsPreview;
