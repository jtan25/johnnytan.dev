import ProjectsPreviewItem from './ProjectsPreviewItem';
import { shallow, mount } from 'enzyme';
import toJSON from 'enzyme-to-json';

const fakeProjects = [
  {
    id: '1',
    type: '1',
    title: 'Example Trello',
    summary:'Organize anything with anyone—for free! Trello is a visual tool for organizing your work and life. Join more than 35 million registered users to organize all your projects at work, at home, or anywhere in between. Whether you’re planning a website design project, vacation, or company off-site, Trello is infinitely customizable and flexible for your every need.',
    logo: 'https://s3.amazonaws.com/owler-image/logo/moody-s_owler_20170810_161002_original.png',
    icon: '',
    tags: ['JavaScript', 'Web', 'SASS'],
    website: 'https://www.trello.com',
    playStore: '',
    appStore: '',
    description:'Organize anything with anyone—for free! Trello is a visual tool for organizing your work and life. Join more than 35 million registered users to organize all your projects at work, at home, or anywhere in between. Whether you’re planning a website design project, vacation, or company off-site, Trello is infinitely customizable and flexible for your every need.',
  },
  {
    id: '2',
    type: '1',
    title: 'Example Instragram',
    summary:'Instagram is a simple way to capture and share the world’s moments. Follow your friends and family to see what they’re up to, and discover accounts from all over the world that are sharing things you love. Join the community of over 1 billion people and express yourself by sharing all the moments of your day — the highlights and everything in between, too.',
    logo: '',
    icon: 'https://is4-ssl.mzstatic.com/image/thumb/Purple113/v4/a4/e5/ee/a4e5eedb-69e1-75c4-f8bd-2c90f08c6854/source/60x60bb.jpg',
    tags: ['Swift', 'Photo & Video', 'VR'],
    website: 'https://www.instagram.com/',
    playStore: 'https://apps.apple.com/us/app/instagram/id389801252',
    appStore: 'https://play.google.com/store/apps/details?id=com.instagram.android&hl=en_US',
    description:'Instagram is a simple way to capture and share the world’s moments. Follow your friends and family to see what they’re up to, and discover accounts from all over the world that are sharing things you love. Join the community of over 1 billion people and express yourself by sharing all the moments of your day — the highlights and everything in between, too.',

  },
]

const fakeProjectPreviewItem = fakeProjects[0];


describe('<ProjectsPreviewItem', () => {

  it('renders and matches the snapshot', () => {
    const wrapper = shallow(<ProjectsPreviewItem props={fakeProjectPreviewItem}/>);
    expect(toJSON(wrapper)).toMatchSnapshot();
  });
})