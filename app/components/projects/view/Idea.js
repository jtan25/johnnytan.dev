import React from 'react';
import ReactMarkdown from 'react-markdown';
import Tag from '../../style/Tag';
import Links from './Links';
import { Section, Title, OneLiner, TagsContainer, DescriptionContainer } from './Styles';




function Idea({props}) {
  const tags = props.tags ? props.tags : [];

  return (
    <Section>
      <Title>💡  {props.title}</Title>
      <TagsContainer>
        {
          tags.map((tag, i) => {
            return <Tag key={i}>{tag}</Tag>
          })
        }
      </TagsContainer>
      <OneLiner>{props.oneLiner}</OneLiner>
      {props.description ? <DescriptionContainer><ReactMarkdown source={props.description}/></DescriptionContainer> : <DescriptionContainer>{props.summary}</DescriptionContainer>}
      <Links project={props}/>
    </Section>
  );
}
export default Idea;