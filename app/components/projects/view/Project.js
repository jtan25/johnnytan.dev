import React from 'react';
import styled from 'styled-components';
import Idea from './Idea';
import Active from './Active';
import Inactive from './Inactive';
import { projects } from '../projects';


const renderProjectById = (id) => {

  const project = projects.filter(project => project.id == id)[0];

  const type = project ? project.type : -1;
  switch(type) {
    case "0":
      return <Active props={project}/>
    case "1":
      return <Idea props={project}/>
    case "2":
      return <Inactive props={project}/>
    default: return <ProjectTitle>404 Not Found</ProjectTitle>;
  }
}

const Section = styled.div`
    display: flex;
    flex-direction:column;
    position: relative;
    margin: 0;
    padding: 6rem 1rem;
`;

const ProjectTitle = styled.div`
    color: rgb(0, 0, 0);
    font-size: 2.2rem;
    font-weight: 900;
    line-height: 1;
    text-align: center;
    margin-bottom: 4rem;
    padding-bottom: 12px;
    width: 100%;
`;


const Project = (props) => {
  return (
    <Section>
      {renderProjectById(props.id)}
    </Section>
  )
}

export default Project;