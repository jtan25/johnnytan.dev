import React from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const ProjectLinks = styled.div`
    padding: 8px 16px;
    display: inline-block;
    text-align: center;
`;

const ProjectLink = styled.a`
    font-weight: 400;
    background-color: transparent;
    outline: none;
    cursor: pointer;
    display: inline-flex;
    justify-content: center;
    align-items: center;
    transition: all .25s;
    margin: 1em;
`;


function Links({project}) {
  const webLink = project.website ? project.website : project.directLink;
  return (
    <ProjectLinks>
    {
      webLink ?
      <ProjectLink href={project.website} style={{textDecoration: 'none', cursor:'pointer'}}>
        <FontAwesomeIcon icon={['fa', 'globe']} color="blue" width="24" />
      </ProjectLink>
      : ""
    }
    {
      project.playStore ?
      <ProjectLink href={project.playStore} style={{textDecoration: 'none', cursor:'pointer'}}>
        <FontAwesomeIcon icon={['fab', 'android']} color="#a4c639" width="24" />
      </ProjectLink>
      : ""
    }
    {
      project.appStore ?
      <ProjectLink href={project.appStore} style={{textDecoration: 'none', cursor:'pointer'}}>
        <FontAwesomeIcon icon={['fab', 'apple']} color="#7D7D7D" width="24" />
      </ProjectLink>
      : ""
    }
  </ProjectLinks>
  )
}

export default Links;