import React from 'react';
import styled from 'styled-components';
import ProjectsDirectoryItem from './ProjectsDirectoryItem';
import { projects } from '../projects';


const Section = styled.div`
    display: flex;
    flex-direction:column;
    position: relative;
    margin: 0;
    padding: 6rem 1rem;
`;

const ProjectsDirectoryHeader = styled.div`
    width: 100%;
`;

const ProjectsDirectoryTitle = styled.div`
    color: rgb(0, 0, 0);
    font-size: 2.2rem;
    font-weight: 900;
    line-height: 1;
    text-align: left;
    margin-bottom: 4rem;
    padding-bottom: 12px;
    border-bottom: 1px solid #777;
    width: 100%;
`;

const ProjectsDirectoryDescription = styled.p`
    color: rgb(85, 85, 85);
    font-size: 1.4rem;
    font-weight: 400;
    line-height: 1.75;
    text-align: left;
    margin: -3rem 0 2rem;
    display: flex;
    flex-direction:column;
`;

const ProjectSectionContainer = styled.div`
    padding: 15px 0;
    width: 100%;
`;

const ProjectsDirectoryMessage = styled.p`
    font-size: 1.3rem;
    font-weight: 400;
    line-height: 1.75;
    text-align: center;
`;

function ProjectsDirectory() {
  return (
    <Section>
      <ProjectsDirectoryHeader>
        <ProjectsDirectoryTitle>
          Projects Directory
        </ProjectsDirectoryTitle>
        <ProjectsDirectoryDescription>
          Listed below are projects (not under NDA) that I am working on, have worked on, gave up on, and a backlog of ideas to explore. 
        </ProjectsDirectoryDescription>
      </ProjectsDirectoryHeader>

      <ProjectSectionContainer>
        <ProjectsDirectoryTitle>
          🚢 Active, Ongoing Projects
        </ProjectsDirectoryTitle>
        <ProjectsDirectoryDescription>
          Projects that I am actively working on. Some are in production and some are still being tested.
        </ProjectsDirectoryDescription>
        {
          projects.filter(project => project.type == 0).reverse().map((project,i) => <ProjectsDirectoryItem props={project} key={i}/>)
        }
      </ProjectSectionContainer>

      <ProjectSectionContainer>
        <ProjectsDirectoryTitle>
          💡 Idea, Seed Stage Projects
        </ProjectsDirectoryTitle>
          <ProjectsDirectoryDescription>
            Ideas or seed stage projects that have been on my mind.
          </ProjectsDirectoryDescription>
          {
            projects.filter(project => project.type == 1).reverse().map((project,i) => <ProjectsDirectoryItem props={project} key={i}/>)
          }
      </ProjectSectionContainer>

      <ProjectSectionContainer>
        <ProjectsDirectoryTitle>
          ⚰️ Inactive, Dead Projects
        </ProjectsDirectoryTitle>
        <ProjectsDirectoryDescription>
            Projects that are no longer updated. Once upon a time active or never completed.
        </ProjectsDirectoryDescription>
        {
          projects.filter(project => project.type == 2).reverse().map((project,i) => <ProjectsDirectoryItem props={project} key={i}/>)
        }
      </ProjectSectionContainer>
    </Section>
  )
}

export default ProjectsDirectory;