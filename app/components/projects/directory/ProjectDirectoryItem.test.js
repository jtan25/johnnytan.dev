import ProjectDirectoryItem from './ProjectsDirectoryItem';
import { shallow, mount } from 'enzyme';
import toJSON from 'enzyme-to-json';


const fakeActiveProjects = [
  {
    'id': "1",
    'title': 'Example project Slack',
    'oneLiner': 'Meeting productivity, Meetings Done Right',
    'tags': ['JavaScript', 'React', 'SASS', 'Web']
  },
  {
    'id': "2",
    'title': 'Example project Instagram',
    'oneLiner': 'Hello World',
    'tags': ['React-Native', 'Android', 'Consumer', 'Web', 'Production']
  }
];

const fakeProjectDirectoryItem = fakeActiveProjects[0];


describe('<ProjectDirectoryItem', () => {

  it('renders and matches the snapshot', () => {
    const wrapper = shallow(<ProjectDirectoryItem props={fakeProjectDirectoryItem}/>);
    expect(toJSON(wrapper)).toMatchSnapshot();
  });
})