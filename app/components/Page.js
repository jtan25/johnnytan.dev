import React, { Component } from 'react';
import styled, { ThemeProvider, createGlobalStyle  } from 'styled-components';
import Meta from './Meta';
import Navbar from './navbar/Navbar';
import { library, config, dom  } from '@fortawesome/fontawesome-svg-core';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { faCircleNotch, faGlobe, faEnvelope } from '@fortawesome/free-solid-svg-icons';

config.autoAddCss = false;
library.add(fab, faCircleNotch, faGlobe, faEnvelope);

const theme = {
  red: '#FF0000',
  blue: '#3b5998',
  black: '#393939',
  grey: '#3A3A3A',
  lightgrey: '#E1E1E1',
  offWhite: '#EDEDED',
  maxWidth: '1000px',
  bs: '0 12px 24px 0 rgba(0, 0, 0, 0.09)',
};

const StyledPage = styled.div`
  background: white;
  color: ${props => props.theme.black};
`;

const Inner = styled.div`
  max-width: ${props => props.theme.maxWidth};
  margin: 0 auto;
  padding: 2rem;
`;

createGlobalStyle`
  @font-face {
    font-family: 'Lora';
    src: url('/static/fonts/Lora-Regular.ttf');
    src: url('/static/fonts/Lora-Bold.ttf');
    src: url('/static/fonts/Lora-BoldItalic.ttf');
    src: url('/static/fonts/Lora-Italic.ttf');
  }
  html {
    box-sizing: border-box;
    font-size: 10px;
  }
  *, *:before, *:after {
    box-sizing: inherit;
  }
  body {
    padding: 0;
    margin: 0;
    font-size: 1.5rem;
    line-height: 2;
    font-family: 'Lora';
  }
  a {
    text-decoration: none;
    color: ${theme.blue};
  }
  ${dom.css()}
`;

class Page extends Component {
  render() {
    return (
      <ThemeProvider theme={theme}>
      <StyledPage>
        <Meta/>
        <Navbar/>
        <Inner>{this.props.children}</Inner>
      </StyledPage>
      </ThemeProvider>
    )
  }
}

export default Page;