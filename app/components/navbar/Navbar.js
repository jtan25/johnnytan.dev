import React from 'react';
import Link from 'next/link';
import styled from 'styled-components';
import { withRouter } from 'next/router'

const NavStyles = styled.ul`
  margin: 0;
  padding: 0;
  display: flex;
  justify-self: end;
  font-size: 2rem;
  justify-content: center;
  position: fixed; /* Set the navbar to fixed position */
  top: 0; /* Position the navbar at the top of the page */
  width: 100%; /* Full width */
  background: white;
  z-index: 10;
  padding-top: 1rem;
  padding-bottom: 1rem;
  @media (max-width: 1300px) {
    border-top: 1px solid ${props => props.theme.lightgrey};
    width: 100%;
    justify-content: center;
    font-size: 1.5rem;
  }
`;

const NavLink = styled.a`
    padding: 1rem 3rem;
    display: flex;
    align-items: center;
    position: relative;
    text-transform: uppercase;
    font-weight: 900;
    font-size: 1em;
    background: none;
    border: 0;
    text-decoration: none;
    cursor: pointer;

    color: ${props => props.theme.black};
    font-weight: 800;
    @media (max-width: 700px) {
      font-size: 16px;
      padding: 0 10px;
    }
    &:after {
      height: 2px;
      background: blue;
      content: '';
      width: ${props => props.active ? 'calc(100% - 60px)'  : 0};;
      position: absolute;
      transform: translateX(-50%);
      transition: width 0.4s;
      transition-timing-function: cubic-bezier(1, -0.65, 0, 2.31);
      left: 50%;
      margin-top: 2rem;
    }
    &:hover,
    &:focus {
      outline: none;
      &:after {
        width: calc(100% - 60px);
      }
  }
`

let ActiveLink = ({ router, children, ...props }) => {
  const child = React.Children.only(children);
  return (
    <Link  {...props}>
      {React.cloneElement(child, { active : router.asPath === props.href })}
    </Link>
  )
}

ActiveLink = withRouter(ActiveLink);

const Navbar = () => {
  return (
    <NavStyles>
      {
        <>
          <ActiveLink href="/">
            <NavLink>Home</NavLink>
          </ActiveLink>
          <ActiveLink href="/projects">
            <NavLink>Projects</NavLink>
          </ActiveLink>
        </>
      }
    </NavStyles>
  )
}

export default Navbar;