import React from 'react';
import styled from 'styled-components';
import SocialMediaLinks from '../social/SocialMediaLinks';


const AboutContainer = styled.div`
  background: white;
  max-width: 100%;
  z-index: 1;
  animation: 'fadeFromAbove' 1s ease 0s 1 normal none;
`;

const AboutName = styled.h1`
  font-size: 3.4rem;
  font-weight: 900;
  line-height: 1.5;
  text-align: center;
  margin-bottom: .5rem;
  color: ${props => props.theme.black};
`;

const AboutTitle = styled.h2`
  color: rgb(0, 0, 0);
  font-size: 1.25rem;
  font-weight: 900;
  line-height: 1;
  text-align: center;
  margin-bottom: .5rem;
`;

const AboutDescriptionContainer = styled.div`
    font-size: 1.2rem;
    font-weight: 400;
    line-height: 1.75;
    text-align: center;
    color: ${props => props.theme.black};
    margin-top: 2rem;
    display: block;
    max-width: 100%;
`;

const AboutDescription = styled.div`
    max-width: 570px;
    color: inherit;
    white-space: pre-wrap;
    line-height: inherit;
    display: inline-block;
`;

const Section = styled.div`
  min-height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
  margin: 0;
  -ms-flex-pack: center;
  -ms-flex-align: center;
`;


const About = () => {
  return (
    <Section>
      <AboutContainer>
        <AboutName>Hello 👋 I'm Johnny.</AboutName>
        <AboutDescriptionContainer>
          <AboutDescription>
            I am a product focused software developer who spends too much time  💤 & 🔨.
          </AboutDescription>
        </AboutDescriptionContainer>
        <SocialMediaLinks/>
    </AboutContainer>
  </Section>
  )
}

export default About;
